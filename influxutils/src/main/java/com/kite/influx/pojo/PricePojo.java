/**
 * 
 */
package com.kite.influx.pojo;

import java.io.Serializable;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

/**
 * @author Deep
 *
 */

@Measurement(name = "Price")
public class PricePojo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7125339500775205485L;

	@Column(name = "RouteID", tag = true)
	private String RouteID = null;
	
	@Column(name = "MinPrice")
	private Double minPrice;
	
	@Column(name = "MaxPrice")
	private Double maxPrice;
	
	@Column(name = "RMinus")
	private Double rMinus;
	
	@Column(name = "RPlus")
	private Double rPlus;
	
	@Column(name = "SurgePrice")
	private Double surgePrice;
	
	@Column(name = "CustomerName")
	private Double CustomerName;

	public Double getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(Double customerName) {
		CustomerName = customerName;
	}

	public String getRouteID() {
		return RouteID;
	}

	public void setRouteID(String routeID) {
		RouteID = routeID;
	}

	public Double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}

	public Double getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(Double maxPrice) {
		this.maxPrice = maxPrice;
	}

	public Double getrMinus() {
		return rMinus;
	}

	public void setrMinus(Double rMinus) {
		this.rMinus = rMinus;
	}

	public Double getrPlus() {
		return rPlus;
	}

	public void setrPlus(Double rPlus) {
		this.rPlus = rPlus;
	}

	public Double getSurgePrice() {
		return surgePrice;
	}

	public void setSurgePrice(Double surgePrice) {
		this.surgePrice = surgePrice;
	}

	@Override
	public String toString() {
		return "PricePojo [RouteID=" + RouteID + ", minPrice=" + minPrice + ", maxPrice=" + maxPrice + ", rMinus="
				+ rMinus + ", rPlus=" + rPlus + ", surgePrice=" + surgePrice + ", CustomerName=" + CustomerName + "]";
	}
}
