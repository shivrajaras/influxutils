/**
 * 
 */
package com.kite.influx.pojo;

import java.io.Serializable;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

/**
 * @author shivraj
 *
 */
@Measurement(name = "OrderCount")
public class OrdersCount implements Serializable{

    private static final long serialVersionUID = -4224888876426542413L;

    @Column(name = "RouteID", tag = true)
    private String RouteID = null;
    
    @Column(name = "TotalOrders")
    private Integer totalOrders = null;

    @Column(name = "PendingOrders")
    private Integer pendingOrders = null;

    /**
     * @return the routeID
     */
    public String getRouteID() {
        return RouteID;
    }

    /**
     * @param routeID the routeID to set
     */
    public void setRouteID(String routeID) {
        RouteID = routeID;
    }

    /**
     * @return the totalOrders
     */
    public Integer getTotalOrders() {
        return totalOrders;
    }

    /**
     * @param totalOrders the totalOrders to set
     */
    public void setTotalOrders(Integer totalOrders) {
        this.totalOrders = totalOrders;
    }

    /**
     * @return the pendingOrders
     */
    public Integer getPendingOrders() {
        return pendingOrders;
    }

    /**
     * @param pendingOrders the pendingOrders to set
     */
    public void setPendingOrders(Integer pendingOrders) {
        this.pendingOrders = pendingOrders;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((RouteID == null) ? 0 : RouteID.hashCode());
	result = prime * result + ((pendingOrders == null) ? 0 : pendingOrders.hashCode());
	result = prime * result + ((totalOrders == null) ? 0 : totalOrders.hashCode());
	return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	OrdersCount other = (OrdersCount) obj;
	if (RouteID == null) {
	    if (other.RouteID != null)
		return false;
	} else if (!RouteID.equals(other.RouteID))
	    return false;
	if (pendingOrders == null) {
	    if (other.pendingOrders != null)
		return false;
	} else if (!pendingOrders.equals(other.pendingOrders))
	    return false;
	if (totalOrders == null) {
	    if (other.totalOrders != null)
		return false;
	} else if (!totalOrders.equals(other.totalOrders))
	    return false;
	return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "OrdersCount [RouteID=" + RouteID + ", totalOrders=" + totalOrders + ", pendingOrders=" + pendingOrders
		+ "]";
    }
    
    
}
