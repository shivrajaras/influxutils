package com.kite.influx.pojo;

import java.io.Serializable;
import java.util.Date;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name = "Surge")
public class SurgePojo implements Serializable {
	private static final long serialVersionUID = -987845611123112345L;

	@Column(name = "RouteID", tag = true)
	private String RouteID = null;
	
	@Column(name = "SurgeRate")
	private Double SurgeRate;
	
	@Column(name = "SurgeTime")
	private String SurgeTime;
	
	@Column(name = "OldSurgeRate")
	private Double OldSurgeRate;
	
	@Column(name = "diffRate")
	private Double diffRate;
	
	@Column(name = "time")
    private String time = null;

	public String getRouteID() {
		return RouteID;
	}

	public void setRouteID(String routeID) {
		RouteID = routeID;
	}

	public Double getSurgeRate() {
		return SurgeRate;
	}

	public void setSurgeRate(Double surgeRate) {
		SurgeRate = surgeRate;
	}

	public String getSurgeTime() {
		return SurgeTime;
	}

	public void setSurgeTime(String surgeTime) {
		SurgeTime = surgeTime;
	}

	public Double getDiffRate() {
		return diffRate;
	}

	public void setDiffRate(Double diffRate) {
		this.diffRate = diffRate;
	}

	public Double getOldSurgeRate() {
		return OldSurgeRate;
	}

	public void setOldSurgeRate(Double oldSurgeRate) {
		OldSurgeRate = oldSurgeRate;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	
	
	
}
