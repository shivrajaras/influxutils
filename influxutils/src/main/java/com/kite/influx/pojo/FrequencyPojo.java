/**
 * 
 */
package com.kite.influx.pojo;

import java.io.Serializable;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

/**
 * @author Deep
 *
 */
@Measurement(name = "TimerFrequency")
public class FrequencyPojo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2692464475578185953L;

	@Column(name = "RouteID", tag = true)
    private String RouteID = null;
	
	@Column(name = "IncFrequency")
    private Integer incfrequency = null;
    
    @Column(name = "DecFrequency")
    private Integer decfrequency = null;
    
    @Column(name = "IncFrequency2")
    private Integer incfrequency2 = null;
    
    @Column(name = "DecFrequency2")
    private Integer decfrequency2 = null;

	/**
	 * @return the routeID
	 */
	public String getRouteID() {
		return RouteID;
	}

	/**
	 * @param routeID the routeID to set
	 */
	public void setRouteID(String routeID) {
		RouteID = routeID;
	}

	/**
	 * @return the incfrequency
	 */
	public Integer getIncfrequency() {
		return incfrequency;
	}

	/**
	 * @param incfrequency the incfrequency to set
	 */
	public void setIncfrequency(Integer incfrequency) {
		this.incfrequency = incfrequency;
	}

	/**
	 * @return the decfrequency
	 */
	public Integer getDecfrequency() {
		return decfrequency;
	}

	/**
	 * @param decfrequency the decfrequency to set
	 */
	public void setDecfrequency(Integer decfrequency) {
		this.decfrequency = decfrequency;
	}

	/**
	 * @return the incfrequency2
	 */
	public Integer getIncfrequency2() {
		return incfrequency2;
	}

	/**
	 * @param incfrequency2 the incfrequency2 to set
	 */
	public void setIncfrequency2(Integer incfrequency2) {
		this.incfrequency2 = incfrequency2;
	}

	/**
	 * @return the decfrequency2
	 */
	public Integer getDecfrequency2() {
		return decfrequency2;
	}

	/**
	 * @param decfrequency2 the decfrequency2 to set
	 */
	public void setDecfrequency2(Integer decfrequency2) {
		this.decfrequency2 = decfrequency2;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FrequencyPojo [RouteID=" + RouteID + ", incfrequency=" + incfrequency + ", decfrequency=" + decfrequency
				+ ", incfrequency2=" + incfrequency2 + ", decfrequency2=" + decfrequency2 + "]";
	}
}
