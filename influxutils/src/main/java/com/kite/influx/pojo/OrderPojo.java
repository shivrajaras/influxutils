/**
 * 
 */
package com.kite.influx.pojo;

import java.io.Serializable;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

/**
 * @author shivraj
 *
 */
@Measurement(name = "OpenOrder")
public class OrderPojo implements Serializable {

    private static final long serialVersionUID = -6396243356652429928L;
    // OrderID,RouteID,From,To,TypeOfTruck,TypeOfMaterial,VendorID,MarketID,TypeOfAction,Price,Count
    @Column(name = "OrderID")
    private String OrderID = null;

    @Column(name = "RouteID", tag = true)
    private String RouteID = null;

    @Column(name = "FromLocation", tag = true)
    private String From = null;

    @Column(name = "ToLocation", tag = true)
    private String To = null;

    @Column(name = "TypeOfTruck", tag = true)
    private String TypeOfTruck = null;

    @Column(name = "TypeOfMaterial", tag = true)
    private String TypeOfMaterial = null;

    @Column(name = "VendorID", tag = true)
    private String VendorID = null;

    @Column(name = "MarketID", tag = true)
    private String MarketID = null;

    @Column(name = "TypeOfAction", tag = true)
    private String TypeOfAction = null;

    @Column(name = "Price")
    private Double Price = null;

    @Column(name = "Count")
    private Integer Count = null;

    @Column(name = "time")
    private String time = null;
    
    @Column(name = "CustomerName", tag = true)
    private String CustomerName = null;

    @Column(name = "IncFrequency")
    private Integer incfrequency = null;

    @Column(name = "DecFrequency")
    private Integer decfrequency = null;

    @Column(name = "IncFrequency2")
    private Integer incfrequency2 = null;

    @Column(name = "DecFrequency2")
    private Integer decfrequency2 = null;

    @Column(name = "TotalOrders")
    private Integer totalOrders = null;

    @Column(name = "PendingOrders")
    private Integer pendingOrders = null;

    @Column(name = "DateRequired", tag = true)
    private String dateRequired = null;

    // this vendorname can be accessed using header name 'x-username'
    @Column(name = "VendorName", tag = true)
    private String vendorName = null;

    /**
     * @return the incfrequency
     */
    public Integer getIncfrequency2() {
	return incfrequency2;
    }

    /**
     * @return the vendorName
     */
    public String getVendorName() {
        return vendorName;
    }

    /**
     * @param vendorName the vendorName to set
     */
    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    /**
     * @param incfrequency
     *            the incfrequency to set
     */
    public void setIncfrequency2(Integer incfrequency2) {
	this.incfrequency2 = incfrequency2;
    }

    /**
     * @return the decfrequency
     */
    public Integer getDecfrequency2() {
	return decfrequency2;
    }

    /**
     * @param decfrequency
     *            the decfrequency to set
     */
    public void setDecfrequency2(Integer decfrequency2) {
	this.decfrequency2 = decfrequency2;
    }

    /**
     * @return the incfrequency
     */
    public Integer getIncfrequency() {
	return incfrequency;
    }

    /**
     * @param incfrequency
     *            the incfrequency to set
     */
    public void setIncfrequency(Integer incfrequency) {
	this.incfrequency = incfrequency;
    }

    /**
     * @return the decfrequency
     */
    public Integer getDecfrequency() {
	return decfrequency;
    }

    /**
     * @param decfrequency
     *            the decfrequency to set
     */
    public void setDecfrequency(Integer decfrequency) {
	this.decfrequency = decfrequency;
    }

    /**
     * @return the orderID
     */
    public String getOrderID() {
	return OrderID;
    }

    /**
     * @param orderID
     *            the orderID to set
     */
    public void setOrderID(String orderID) {
	OrderID = orderID;
    }

    /**
     * @return the routeID
     */
    public String getRouteID() {
	return RouteID;
    }

    /**
     * @param routeID
     *            the routeID to set
     */
    public void setRouteID(String routeID) {
	RouteID = routeID;
    }

    /**
     * @return the from
     */
    public String getFrom() {
	return From;
    }

    /**
     * @param from
     *            the from to set
     */
    public void setFrom(String from) {
	From = from;
    }

    /**
     * @return the to
     */
    public String getTo() {
	return To;
    }

    /**
     * @param to
     *            the to to set
     */
    public void setTo(String to) {
	To = to;
    }

    /**
     * @return the typeOfTruck
     */
    public String getTypeOfTruck() {
	return TypeOfTruck;
    }

    /**
     * @param typeOfTruck
     *            the typeOfTruck to set
     */
    public void setTypeOfTruck(String typeOfTruck) {
	TypeOfTruck = typeOfTruck;
    }

    /**
     * @return the typeOfMaterial
     */
    public String getTypeOfMaterial() {
	return TypeOfMaterial;
    }

    /**
     * @param typeOfMaterial
     *            the typeOfMaterial to set
     */
    public void setTypeOfMaterial(String typeOfMaterial) {
	TypeOfMaterial = typeOfMaterial;
    }

    /**
     * @return the vendorID
     */
    public String getVendorID() {
	return VendorID;
    }

    /**
     * @param vendorID
     *            the vendorID to set
     */
    public void setVendorID(String vendorID) {
	VendorID = vendorID;
    }

    /**
     * @return the marketID
     */
    public String getMarketID() {
	return MarketID;
    }

    /**
     * @param marketID
     *            the marketID to set
     */
    public void setMarketID(String marketID) {
	MarketID = marketID;
    }

    /**
     * @return the typeOfAction
     */
    public String getTypeOfAction() {
	return TypeOfAction;
    }

    /**
     * @param typeOfAction
     *            the typeOfAction to set
     */
    public void setTypeOfAction(String typeOfAction) {
	TypeOfAction = typeOfAction;
    }

    /**
     * @return the price
     */
    public Double getPrice() {
	return Price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(Double price) {
	Price = price;
    }

    /**
     * @return the count
     */
    public Integer getCount() {
	return Count;
    }

    /**
     * @param count
     *            the count to set
     */
    public void setCount(Integer count) {
	Count = count;
    }

    /**
     * @return the time
     */
    public String getTime() {
	return time;
    }

    /**
     * @param time
     *            the time to set
     */
    public void setTime(String time) {
	this.time = time;
    }

    /**
     * @return the totalOrders
     */
    public Integer getTotalOrders() {
	return totalOrders;
    }

    /**
     * @param totalOrders
     *            the totalOrders to set
     */
    public void setTotalOrders(Integer totalOrders) {
	this.totalOrders = totalOrders;
    }

    /**
     * @return the pendingOrders
     */
    public Integer getPendingOrders() {
	return pendingOrders;
    }

    /**
     * @param pendingOrders
     *            the pendingOrders to set
     */
    public void setPendingOrders(Integer pendingOrders) {
	this.pendingOrders = pendingOrders;
    }

    /**
     * @return the dateRequired
     */
    public String getDateRequired() {
	return dateRequired;
    }

    /**
     * @param dateRequired
     *            the dateRequired to set
     */
    public void setDateRequired(String dateRequired) {
	this.dateRequired = dateRequired;
    }
    
    /**
     * @return the customerName
     */
    public String getCustomerName() {
		return CustomerName;
	}

    /**
     * @param customerName
     *            the customerName to set
     */
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Count == null) ? 0 : Count.hashCode());
		result = prime * result + ((CustomerName == null) ? 0 : CustomerName.hashCode());
		result = prime * result + ((From == null) ? 0 : From.hashCode());
		result = prime * result + ((MarketID == null) ? 0 : MarketID.hashCode());
		result = prime * result + ((OrderID == null) ? 0 : OrderID.hashCode());
		result = prime * result + ((Price == null) ? 0 : Price.hashCode());
		result = prime * result + ((RouteID == null) ? 0 : RouteID.hashCode());
		result = prime * result + ((To == null) ? 0 : To.hashCode());
		result = prime * result + ((TypeOfAction == null) ? 0 : TypeOfAction.hashCode());
		result = prime * result + ((TypeOfMaterial == null) ? 0 : TypeOfMaterial.hashCode());
		result = prime * result + ((TypeOfTruck == null) ? 0 : TypeOfTruck.hashCode());
		result = prime * result + ((VendorID == null) ? 0 : VendorID.hashCode());
		result = prime * result + ((dateRequired == null) ? 0 : dateRequired.hashCode());
		result = prime * result + ((decfrequency == null) ? 0 : decfrequency.hashCode());
		result = prime * result + ((decfrequency2 == null) ? 0 : decfrequency2.hashCode());
		result = prime * result + ((incfrequency == null) ? 0 : incfrequency.hashCode());
		result = prime * result + ((incfrequency2 == null) ? 0 : incfrequency2.hashCode());
		result = prime * result + ((pendingOrders == null) ? 0 : pendingOrders.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		result = prime * result + ((totalOrders == null) ? 0 : totalOrders.hashCode());
		result = prime * result + ((vendorName == null) ? 0 : vendorName.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderPojo other = (OrderPojo) obj;
		if (Count == null) {
			if (other.Count != null)
				return false;
		} else if (!Count.equals(other.Count))
			return false;
		if (CustomerName == null) {
			if (other.CustomerName != null)
				return false;
		} else if (!CustomerName.equals(other.CustomerName))
			return false;
		if (From == null) {
			if (other.From != null)
				return false;
		} else if (!From.equals(other.From))
			return false;
		if (MarketID == null) {
			if (other.MarketID != null)
				return false;
		} else if (!MarketID.equals(other.MarketID))
			return false;
		if (OrderID == null) {
			if (other.OrderID != null)
				return false;
		} else if (!OrderID.equals(other.OrderID))
			return false;
		if (Price == null) {
			if (other.Price != null)
				return false;
		} else if (!Price.equals(other.Price))
			return false;
		if (RouteID == null) {
			if (other.RouteID != null)
				return false;
		} else if (!RouteID.equals(other.RouteID))
			return false;
		if (To == null) {
			if (other.To != null)
				return false;
		} else if (!To.equals(other.To))
			return false;
		if (TypeOfAction == null) {
			if (other.TypeOfAction != null)
				return false;
		} else if (!TypeOfAction.equals(other.TypeOfAction))
			return false;
		if (TypeOfMaterial == null) {
			if (other.TypeOfMaterial != null)
				return false;
		} else if (!TypeOfMaterial.equals(other.TypeOfMaterial))
			return false;
		if (TypeOfTruck == null) {
			if (other.TypeOfTruck != null)
				return false;
		} else if (!TypeOfTruck.equals(other.TypeOfTruck))
			return false;
		if (VendorID == null) {
			if (other.VendorID != null)
				return false;
		} else if (!VendorID.equals(other.VendorID))
			return false;
		if (dateRequired == null) {
			if (other.dateRequired != null)
				return false;
		} else if (!dateRequired.equals(other.dateRequired))
			return false;
		if (decfrequency == null) {
			if (other.decfrequency != null)
				return false;
		} else if (!decfrequency.equals(other.decfrequency))
			return false;
		if (decfrequency2 == null) {
			if (other.decfrequency2 != null)
				return false;
		} else if (!decfrequency2.equals(other.decfrequency2))
			return false;
		if (incfrequency == null) {
			if (other.incfrequency != null)
				return false;
		} else if (!incfrequency.equals(other.incfrequency))
			return false;
		if (incfrequency2 == null) {
			if (other.incfrequency2 != null)
				return false;
		} else if (!incfrequency2.equals(other.incfrequency2))
			return false;
		if (pendingOrders == null) {
			if (other.pendingOrders != null)
				return false;
		} else if (!pendingOrders.equals(other.pendingOrders))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		if (totalOrders == null) {
			if (other.totalOrders != null)
				return false;
		} else if (!totalOrders.equals(other.totalOrders))
			return false;
		if (vendorName == null) {
			if (other.vendorName != null)
				return false;
		} else if (!vendorName.equals(other.vendorName))
			return false;
		return true;
	}

    @Override
	public String toString() {
		return "OrderPojo [OrderID=" + OrderID + ", RouteID=" + RouteID + ", From=" + From + ", To=" + To
				+ ", TypeOfTruck=" + TypeOfTruck + ", TypeOfMaterial=" + TypeOfMaterial + ", VendorID=" + VendorID
				+ ", MarketID=" + MarketID + ", TypeOfAction=" + TypeOfAction + ", Price=" + Price + ", Count=" + Count
				+ ", time=" + time + ", CustomerName=" + CustomerName + ", incfrequency=" + incfrequency
				+ ", decfrequency=" + decfrequency + ", incfrequency2=" + incfrequency2 + ", decfrequency2="
				+ decfrequency2 + ", totalOrders=" + totalOrders + ", pendingOrders=" + pendingOrders
				+ ", dateRequired=" + dateRequired + ", vendorName=" + vendorName + "]";
	}
    
    

}
