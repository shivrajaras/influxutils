/**
 * 
 */
package com.kite.influx.utils;

/**
 * @author shivraj
 *
 */
public interface Keys {
    
    String Price = "Price" ;
    String Count = "Count";
    String IncFrequency = "IncFrequency";
    String DecFrequency = "DecFrequency";
    String IncFrequency2 = "IncFrequency2";
    String DecFrequency2 = "DecFrequency2";
    String TotalOrders = "TotalOrders";
    String PendingOrders = "PendingOrders";
    String MinPrice = "MinPrice";
    String MaxPrice = "MaxPrice";
    String RMinus = "RMinus";
    String RPlus = "RPlus";
    String SurgePrice = "SurgePrice";
    String OrderID = "OrderID";
  //Anush Start
	String SurgeRate = "SurgeRate";
	String SurgeTime = "SurgeTime";
	String DiffRate = "DiffRate";
	String OldSurgeRate="OldSurgeRate";
	String CustomerName="CustomerName";
	//Anush End
    
}
