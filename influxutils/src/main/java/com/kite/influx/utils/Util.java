
/**
 * 
 */
package com.kite.influx.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;

import com.kite.influx.APS;
import com.kite.influx.pojo.FrequencyPojo;
import com.kite.influx.pojo.OrderPojo;
import com.kite.influx.pojo.OrdersCount;
import com.kite.influx.pojo.PricePojo;
import com.kite.influx.pojo.SurgePojo;

/**
 * @author shivraj
 *
 */
public class Util {

    static final Log log = LogFactory.getLog(APS.class);

    public static Map<String ,String> getTagMap(OrderPojo p){
	Map<String ,String> map = null ;
	Map<String ,String> fmap = new HashMap<String , String>(); 
	try {
	    map = new HashMap<String , String>();
	    
//	    map.put(TagKeys.OrderID, p.getOrderID());
	    map.put(TagKeys.RouteID, p.getRouteID());
	    map.put(TagKeys.MarketID, p.getMarketID());
	    map.put(TagKeys.From, p.getFrom());
	    map.put(TagKeys.To, p.getTo());
	    map.put(TagKeys.TypeOfTruck, p.getTypeOfTruck());
	    map.put(TagKeys.TypeOfMaterial, p.getTypeOfMaterial());
	    map.put(TagKeys.VendorID, p.getVendorID());
	    map.put(TagKeys.TypeOfAction, p.getTypeOfAction());
	    map.put(TagKeys.DateRequired, p.getDateRequired());
	    
	    map.forEach((k,v) ->{
		
		if(v != null && !v.isEmpty() && v != "null") {
		    fmap.put(k, v);
		}
	    });
	    
	} catch (Exception e) {
	    
	}
	return fmap;
	
    }
    public static Map<String, Object> getMap(OrderPojo p) {
	Map<String ,Object> map = null ;
	Map<String ,Object> fmap = new HashMap<String , Object>(); 
	try {
	    map = new HashMap<String , Object>();
	    map.put(Keys.OrderID, p.getOrderID());
	    map.put(Keys.Price, p.getPrice());
	    map.put(Keys.OrderID, p.getOrderID());
	    map.put(Keys.Count, p.getCount());
	    map.put(Keys.CustomerName, p.getCustomerName());
	    map.put(Keys.IncFrequency, p.getIncfrequency());
	    map.put(Keys.DecFrequency, p.getDecfrequency());
	    map.put(Keys.IncFrequency2, p.getIncfrequency2());
	    map.put(Keys.DecFrequency2, p.getDecfrequency2());
	    map.put(Keys.TotalOrders, p.getTotalOrders());
	    map.put(Keys.PendingOrders, p.getPendingOrders());
	    
	    map.forEach((k,v) ->{
		
		if(v != null && v != "null") {
		    fmap.put(k, v);
		}
	    });
	 
	} catch (Exception e) {
	    
	}
	return fmap;
    }
    
    public static List<OrderPojo> toPoJo(QueryResult queryResult){
	InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
   	List<OrderPojo> p = resultMapper.toPOJO(queryResult, OrderPojo.class);
	return p;
    }
    
	public static Map<String, String> getFrequencyTagMap(FrequencyPojo frequencyPojo) {
		Map<String, String> map = null;
		Map<String, String> fmap = new HashMap<String, String>();
		try {
			if (null != frequencyPojo) {
				map = new HashMap<String, String>();
				map.put(TagKeys.RouteID, frequencyPojo.getRouteID());

				map.forEach((k, v) -> {
					if(null != v && !"null".equals(v) && !v.isEmpty()) {
						fmap.put(k, v);
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fmap;
	}
    
   public static Map<String ,Object> getFrequencyMap(FrequencyPojo frequencyPojo) {
	   	Map<String ,Object> map = null ;
		Map<String ,Object> fmap = new HashMap<String , Object>(); 
		try {
			if (null != frequencyPojo) {
				map = new HashMap<String , Object>();
			    map.put(Keys.IncFrequency, frequencyPojo.getIncfrequency());
			    map.put(Keys.DecFrequency, frequencyPojo.getDecfrequency());
			    map.put(Keys.IncFrequency2, frequencyPojo.getIncfrequency2());
			    map.put(Keys.DecFrequency2, frequencyPojo.getDecfrequency2());
			    
			    map.forEach((k,v) ->{				
					if(null != v && !"null".equals(v)) {
					    fmap.put(k, v);
					}
			    });
			}		 
		} catch (Exception e) {
		    e.printStackTrace();
		}
		return fmap;
   }
   
   public static Map<String, String> getPriceTagMap(PricePojo pricePojo) {
		Map<String, String> map = null;
		Map<String, String> fmap = new HashMap<String, String>();
		try {
			if (null != pricePojo) {
				map = new HashMap<String, String>();
				map.put(TagKeys.RouteID, pricePojo.getRouteID());

				map.forEach((k, v) -> {
					if(null != v && !"null".equals(v) && !v.isEmpty()) {
						fmap.put(k, v);
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fmap;
	}
   
   public static Map<String ,Object> getPriceMap(PricePojo pricePojo) {
	   	Map<String ,Object> map = null ;
		Map<String ,Object> fmap = new HashMap<String , Object>(); 
		try {
			if (null != pricePojo) {
				map = new HashMap<String , Object>();
			    map.put(Keys.MinPrice, pricePojo.getMinPrice());
			    map.put(Keys.MaxPrice, pricePojo.getMaxPrice());
			    map.put(Keys.RMinus, pricePojo.getrMinus());
			    map.put(Keys.RPlus, pricePojo.getrPlus());
			    map.put(Keys.SurgePrice, pricePojo.getSurgePrice());
			    map.put(Keys.CustomerName, pricePojo.getCustomerName());
			    
			    map.forEach((k,v) ->{				
					if(null != v && !"null".equals(v)) {
					    fmap.put(k, v);
					}
			    });
			}		 
		} catch (Exception e) {
		    e.printStackTrace();
		}
		return fmap;
  }
   //Anush Start
   public static Map<String ,Object> getSurgeMap(SurgePojo surgePojo) {
	   	Map<String ,Object> map = null ;
		Map<String ,Object> fmap = new HashMap<String , Object>(); 
		try {
			if (null != surgePojo) {
				map = new HashMap<String , Object>();
			    map.put(Keys.SurgeRate, surgePojo.getSurgeRate());
			    map.put(Keys.SurgeTime, surgePojo.getSurgeTime());
			    map.put(Keys.DiffRate, surgePojo.getDiffRate());
			    map.put(Keys.OldSurgeRate, surgePojo.getOldSurgeRate());
			    
			    map.forEach((k,v) ->{				
					if(null != v && !"null".equals(v)) {
					    fmap.put(k, v);
					}
			    });
			}		 
		} catch (Exception e) {
		    e.printStackTrace();
		}
		return fmap;
 }
   public static Map<String, String> getSurgeTagMap(SurgePojo surgePojo) {
		Map<String, String> map = null;
		Map<String, String> fmap = new HashMap<String, String>();
		try {
			if (null != surgePojo) {
				map = new HashMap<String, String>();
				map.put(TagKeys.RouteID, surgePojo.getRouteID());

				map.forEach((k, v) -> {
					if(null != v && !"null".equals(v) && !v.isEmpty()) {
						fmap.put(k, v);
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fmap;
	}
 //Anush End
   
	 public static Map<String, Object> getPriceMap(OrdersCount ordersCount) {
	Map<String, Object> map = new HashMap<String, Object>();
	map.put(Keys.PendingOrders, ordersCount.getPendingOrders() != null ? ordersCount.getPendingOrders() : 0);
	map.put(Keys.TotalOrders, ordersCount.getTotalOrders() != null ? ordersCount.getTotalOrders() : 0);
	return map;
}

public static Map<String, String> getPriceTagMap(OrdersCount ordersCount) {
	Map<String, String> tag = new HashMap<String, String>();
	tag.put(TagKeys.RouteID, ordersCount.getRouteID());
	return tag;
}
}

