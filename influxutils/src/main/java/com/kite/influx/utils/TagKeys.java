/**
 * 
 */
package com.kite.influx.utils;

/**
 * @author shivraj
 *
 */
public interface TagKeys {

//    String OrderID = "OrderID";
    String RouteID = "RouteID";
    String From = "FromLocation";
    String To = "ToLocation";
    String TypeOfTruck = "TypeOfTruck";
    String TypeOfMaterial = "TypeOfMaterial";
    String VendorID = "VendorID";
    String MarketID = "MarketID";
    String TypeOfAction = "TypeOfAction";
    String DateRequired = "DateRequired";
    String VendorName = "VendorName";
    
}
