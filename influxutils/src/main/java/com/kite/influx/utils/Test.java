/**
 * 
 */
package com.kite.influx.utils;

import java.util.ArrayList;
import java.util.List;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;

import com.kite.influx.pojo.OrderPojo;

/**
 * @author shivraj
 *
 */
public class Test {

    static InfluxDB influxDB = InfluxDBFactory.connect("http://kitedb.cloudapp.net:8086", "kite","meratransport");
    static String dbName = "mydb";

    static String query = "insert table id=12,name=\"temp\",place=\"delhi\",price=12.0";

    public static void updateInfluxOrder(String influxStr){
           influxDB.write(dbName, "autogen", InfluxDB.ConsistencyLevel.ONE, influxStr );
           
    }
    
    private static List<OrderPojo>  getLatestPriceForAOrderGroupByVendor(String OrderID) {
	//'ORD17005003'
	String ss = "select VendorID,OrderID ,LAST(Price) as Price from OpenOrder WHERE OrderID = \'"+OrderID+"\' Group By VendorID;";
	Query q = new Query(ss, DbNames.mydb);
	QueryResult queryResult = influxDB.query(q);
	InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
	List<OrderPojo> l = resultMapper.toPOJO(queryResult, OrderPojo.class);
	//System.out.println(l);
	return l;
	}
    
    private static List<OrderPojo>  getLatestCountForVendor(String vid){
	//String count = "select SUM(Count) as Count from OpenOrder WHERE OrderID = \'"+OrderID+"\' Group By VendorID;";
	String count = "select SUM(Count) as Count from OpenOrder WHERE VendorID =\'"+vid+"\'";
	Query q = new Query(count, DbNames.mydb);
	QueryResult queryResult = influxDB.query(q);
	InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
	List<OrderPojo> l = resultMapper.toPOJO(queryResult, OrderPojo.class);
	//System.out.println(l);
	return l;
    }
    
    
    public static void main(String[] args) {
	
	
	String count = "select * from OpenOrder WHERE RouteID ='1'";
	Query q = new Query(count, DbNames.mydb);
	QueryResult queryResult = influxDB.query(q);
	InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
	List<OrderPojo> l = resultMapper.toPOJO(queryResult, OrderPojo.class);
	
	System.out.println(l);
	//getLatestPriceForAOrderGroupByVendor("ORD17005003");
    
	//List<OrderPojo> pojolist = new ArrayList<OrderPojo>();
	//List<OrderPojo>  price = getLatestPriceForAOrderGroupByVendor("ORD17005003");
	//List<OrderPojo>  count = getLatestCountForVendor("ORD17005003");
	
	
	/*price.forEach((p) ->{
	    List<OrderPojo> countpojo= getLatestCountForVendor(p.getVendorID());
	    if(!countpojo.isEmpty()) {
		    OrderPojo po = new OrderPojo();
		    po.setOrderID(p.getOrderID());
		    po.setVendorID(p.getVendorID());
		    po.setCount(countpojo.get(0).getCount());
		    po.setPrice(p.getPrice());
		    po.setTime(String.valueOf(System.currentTimeMillis()));
		    pojolist.add(po);
		}
		
		
	    
	});
	System.out.println(pojolist);
	
	price.forEach((p) ->{
	    count.forEach((c) ->{
		if(c.getVendorID() == p.getVendorID()) {
		    OrderPojo po = new OrderPojo();
		    po.setVendorID(p.getVendorID());
		    po.setCount(c.getCount());
		    po.setPrice(p.getPrice());
		    po.setTime(String.valueOf(System.currentTimeMillis()));
		    pojolist.add(po);
		}
		
	    });
	});
	System.out.println(pojolist);
	
	
	price.forEach((l) ->{
	    l.getSeries().forEach((s) ->{
		OrderPojo p = new OrderPojo();
		p.setVendorID(s.getTags().get("VendorID"));
		p.setOrderID((String) s.getValues().get(0).get(1));
		p.setTime((String) s.getValues().get(0).get(0));
		p.setPrice(String.valueOf(s.getValues().get(0).get(2)));
		pojolist.add(p);
	    });
	});
	
	  
    }*/
    }
}
