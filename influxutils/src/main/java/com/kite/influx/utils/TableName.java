/**
 * 
 */
package com.kite.influx.utils;

/**
 * @author shivraj
 *
 */
public interface TableName {

    String OpenOrder = "OpenOrder";
    String TimerFrequency = "TimerFrequency";
    String Price = "Price";
    String ConfirmOrder = "ConfirmOrder";
    String OrderCount = "OrderCount";
    String Surge="Surge";
}
