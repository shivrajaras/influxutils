

package com.kite.influx;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDB.ConsistencyLevel;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.dto.QueryResult.Result;
import org.influxdb.dto.QueryResult.Series;
import org.influxdb.impl.InfluxDBResultMapper;
import com.kite.influx.APS;
import com.kite.influx.pojo.FrequencyPojo;
import com.kite.influx.pojo.OrderPojo;
import com.kite.influx.pojo.OrdersCount;
import com.kite.influx.pojo.PricePojo;
import com.kite.influx.pojo.SurgePojo;
import com.kite.influx.templateImp.InfluxWriteImplenetation;
import com.kite.influx.utils.DbNames;
import com.kite.influx.utils.TableName;
import com.kite.influx.utils.Util;

/**
 * @author shivraj
 *
 */


public final class APS {

    static String host = null;
    static String userName = null;
    static String password = null;
    static String dbName = DbNames.mydb;
    static String retentionPolicyName = "meratransport";
    static final Log log = LogFactory.getLog(APS.class);
  //  public static InfluxDB influxDB = null;
   /* static Properties pro = null;
    static {
	pro = new Properties();
	try {
	    pro.load(APS.class.getResourceAsStream("/influx.properties"));
	    userName = pro.getProperty("influx.username");
	    password = pro.getProperty("influx.password");
	    dbName = pro.getProperty("influx.database");
	    host = pro.getProperty("influx.url");
	    System.setProperties(pro);
	    System.out.println("********************************************");
	    System.out.println(""+userName +" "+password +" "+dbName +" "+host);
	    System.out.println("********************************************");
	    influxDB = InfluxDBFactory.connect(host,userName,password);
	} catch (IOException e) {
	    log.error("properies not found");
	}
    }*/

   public static InfluxDB influxDB = InfluxDBFactory.connect("http://kitedb.cloudapp.net:8086", "kite",
	    "meratransport");
   

    public synchronized static void writeData(Point p) {
	log.info("writing one point");
	influxDB.write(dbName, retentionPolicyName, p);
    }

    public static List<String> listDatabases() {
	log.info("getting list of databases");
	return influxDB.describeDatabases();
    }

    public static void createDataBase(String name) {
	// CREATE RETENTION POLICY meratransport ON APS DURATION INF REPLICATION
	// 1 DEFAULT
	log.info("creating database with meratransport retention policy");
	influxDB.setConsistency(ConsistencyLevel.ONE);
	influxDB.setRetentionPolicy(retentionPolicyName);
	influxDB.createDatabase(name);
    }

    public synchronized static void deleteDataBase(String name) {
	influxDB.deleteDatabase(name);
    }
    

    public synchronized static void deleteTable(String table) {
	log.info("deleting table with name: " + table);
	influxDB.query(new Query("drop MEASUREMENT " + table, dbName));
    }

    
    public synchronized static void writeData(Map<String, Object> map, Map<String, String> tags, String table) {
	try {
	    log.info("writing data into table or measurements with name " + table);
	    Point p = Point.measurement(table).fields(map).tag(tags)
		    .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS).build();
	    influxDB.write(dbName, retentionPolicyName, p);
	} catch (Exception e) {
		e.printStackTrace();
	    log.error("exception while writing data into table " + table);
	    log.info("" + e.getMessage());
	}
    }

    public static QueryResult getResults(String q) {
	QueryResult response = null;
	try {
	    Query p = new Query(q, DbNames.mydb);
	    log.info("getting results " + p.getCommand());
	    response = influxDB.query(p);
	} catch (Exception e) {
	    log.error("error while geting results " + e.getMessage());
	}
	return response;

    }

    public static List<OrderPojo> getResultsByOrderID(String OrderId) {
	List<OrderPojo> response = null;
	try {
	    log.info("getting results by orderID " + OrderId);
	    response = Util.toPoJo(influxDB.query(new Query(
		    "select * from " + TableName.OpenOrder + " where OrderID = \'" + OrderId + "\'", DbNames.mydb)));
	} catch (Exception e) {
	    log.error("error while geting results " + e.getMessage());
	}
	return response;

    }

    public static List<OrderPojo> getResultsByOrderIDandVid(String OrderId, String vid) {
	List<OrderPojo> response = null;
	try {
	    log.info("getting results by orderID " + OrderId + "and VendorID " + vid);
	    Query q = new Query("select * from " + TableName.OpenOrder + "" + " where "
		    + "OrderID = \'" + OrderId + "\' " + "  AND  " + " VendorID =\'" + vid + "\'", DbNames.mydb);
	    log.info("query :"+q.getCommand()+"\n"+q.getCommandWithUrlEncoded());
	    response = Util.toPoJo(influxDB.query(q));
	} catch (Exception e) {
	    log.error("error while geting results " + e.getMessage());
	}
	return response;

    }
    
    /**
     * @param marketID
     * @return
     */
    public static List<OrderPojo> getResultsByMarketID(String marketID) {
   	List<OrderPojo> response = null;
   	try {
   	    log.info("getting results by MarketID " + marketID);
   	    response = Util.toPoJo(influxDB.query(new Query(
   		    "select * from " + TableName.OpenOrder + " where MarketID = \'" + marketID + "\'", DbNames.mydb)));
   	} catch (Exception e) {
   	    log.error("error while geting results " + e.getMessage());
   	}
   	return response;

       }
    
    /**
     * @param routeID
     * @return
     */
    public static List<OrderPojo> getResultsByRouteID(String routeID) {
   	List<OrderPojo> response = null;
   	try {
   	    log.info("getting results by RouteID " + routeID);
   	    response = Util.toPoJo(influxDB.query(new Query(
   		    "select * from " + TableName.OpenOrder + " where RouteID = \'" + routeID + "\'", DbNames.mydb)));
   	} catch (Exception e) {
   	    log.error("error while geting results " + e.getMessage());
   	}
   	return response;

       }//getLatestPriceForAOrderGroupByVendor
    
    
    /**
     * @param typeOfActiviti
     * @return
     */
    public static List<OrderPojo> getResultsByTypeOfActivit(String typeOfActiviti) {
   	List<OrderPojo> response = null;
   	try {
   	    log.info("getting results by TypeOfAction " + typeOfActiviti);
   	    response = Util.toPoJo(influxDB.query(new Query(
   		    "select * from " + TableName.OpenOrder + " where TypeOfAction = \'" + typeOfActiviti + "\'", DbNames.mydb)));
   	} catch (Exception e) {
   	    log.error("error while geting results " + e.getMessage());
   	}
   	return response;

       }
    
    /**
     * @param routeID
     * @return
     */
    public static List<OrderPojo> getLatestPriceAndCountForARouterGroupByVendor(String routeID) {
   	List<OrderPojo> response = null;
   	try {
   	    log.info("getting results by routeID " + routeID);
   	//   Query query = new Query("select  last(Price) as Price, SUM(Count) as Count from OpenOrder  where RouteID = \'"+routeID+"\' group by VendorID", DbNames.mydb);
   	   response = new InfluxWriteImplenetation().getLatestPriceForAOrderGroupByVendor(routeID);
   	} catch (Exception e) {
   	    log.error("error while geting results " + e.getMessage());
   	}
   	return response;

       }
    
   /* *//**
     * @param OrderID
     * @return
     *//*
    public static List<OrderPojo> getLatestPriceForAOrderGroupByVendorByOrderId(String OrderID) {
   	List<OrderPojo> response = null;
   	try {
   	    log.info("getting results by OrderID " + OrderID);
   	   response = new InfluxWriteImplenetation().getLatestPriceForAOrderGroupByVendor(OrderID);
   	} catch (Exception e) {
   	    log.error("error while geting results " + e.getMessage());
   	}
   	return response;

       }*/
    
    /**
     * @param routeID
     * @return
     */
	public static OrderPojo getLatestPrice(String routeID) {
		OrderPojo order = null;
		try {
			log.info("Fetching latest price for the routeID: " + routeID);
			 Query query = new Query("select last(Price) as Price from " + TableName.OpenOrder + " where RouteID = \'"+routeID+"\' and VendorID = \'-\'", DbNames.mydb);
			 log.info("Query: " + query.getCommand());
			 QueryResult queryResult = influxDB.query(query);
			 System.out.println(queryResult);
			 InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
			 List<OrderPojo> orders = resultMapper.toPOJO(queryResult, OrderPojo.class);
			 if (orders.size() > 0) {
				 order = orders.get(0);
			 }
		} catch (Exception e) {
			log.error("error while geting results " + e.getMessage());
		}
		return order;
	}
	
	public static OrderPojo getLatestConfirmPrice(String routeID) {
		OrderPojo order = null;
		try {
			log.info("Fetching latest price for the routeID: " + routeID);
			 Query query = new Query("select last(Price) as Price from " + TableName.ConfirmOrder + " where RouteID = \'"+routeID+"\' and VendorID = \'-\'", DbNames.mydb);
			 log.info("Query: " + query.getCommand());
			 QueryResult queryResult = influxDB.query(query);
			 System.out.println(queryResult);
			 InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
			 List<OrderPojo> orders = resultMapper.toPOJO(queryResult, OrderPojo.class);
			 if (orders.size() > 0) {
				 order = orders.get(0);
			 } else {
				 order = getLatestPrice(routeID);
			 }
		} catch (Exception e) {
			log.error("error while geting results " + e.getMessage());
		}
		return order;
	}
	
	/**
     * @param routeID
     * @return
     */
	//Anush Start
	public static PricePojo getMinTargetPrice(String routeID) {
		PricePojo order = null;
		try {
			log.info("Fetching latest price for the routeID: " + routeID);
			 Query query = new Query("select RMinus from " + TableName.Price + " where RouteID = \'"+routeID+"\' ", DbNames.mydb);
			 log.info("Query: " + query.getCommand());
			 QueryResult queryResult = influxDB.query(query);
			 System.out.println(queryResult);
			 InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
			 List<PricePojo> orders = resultMapper.toPOJO(queryResult, PricePojo.class);
			 System.out.println("check value ");
			 System.out.println(orders);
			 if (orders.size() > 0) {
				 order = orders.get(0);
			 }
			 System.out.println(order);
		} catch (Exception e) {
			log.error("error while geting results " + e.getMessage());
		}
		return order;
	}
	
	public static SurgePojo getLatestSurgePrice(final String routeID) {
		SurgePojo surge = null;
		try {
			log.info("Fetching latest Surge  price for the routeID: " + routeID);
			 Query query = new Query("select last(SurgeRate)  from " + TableName.Surge + " where RouteID = \'"+routeID+"\'", DbNames.mydb);
			 log.info("Query: " + query.getCommand());
			 QueryResult queryResult = influxDB.query(query);
			 System.out.println(queryResult);
			 InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
			 List<SurgePojo> orders = resultMapper.toPOJO(queryResult, SurgePojo.class);
			 if (orders.size() > 0) {
				 surge = orders.get(0);
			 } 
		} catch (Exception e) {
			log.error("error while geting results " + e.getMessage());
		}
		return surge;
	}
	
	//Anush End
	/**
     * @param routeID
     * @return
     */
	public static OrderPojo getLatestPrice(final String routeID, final String vendorID) {
		OrderPojo order = null;
		try {
			log.info("Fetching latest price for the routeID: " + routeID);
			 Query query = new Query("select last(Price) as Price from " + TableName.OpenOrder + " where RouteID = \'"+routeID+"\' and VendorID = \'" + vendorID + "\' ", DbNames.mydb);
			 log.info("Query: " + query.getCommand());
			 QueryResult queryResult = influxDB.query(query);
			 System.out.println(queryResult);
			 InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
			 List<OrderPojo> orders = resultMapper.toPOJO(queryResult, OrderPojo.class);
			 if (orders.size() == 0) {
				 order = getLatestPrice(routeID);
			 } else {
				 order = orders.get(0);
			 }
		} catch (Exception e) {
			log.error("error while geting results " + e.getMessage());
		}
		return order;
	}
	
	public static FrequencyPojo getFrequency(final String routeId) {
		FrequencyPojo frequency = null;
		try {
			if (null != routeId) {
				 log.info("Fetching frequency for the routeID: " + routeId);
				 Query query = new Query("select * from " + TableName.TimerFrequency + " where RouteID = \'"+routeId+"\'", DbNames.mydb);
				 log.info("Query: " + query.getCommand());
				 QueryResult queryResult = influxDB.query(query);
				 InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
				 List<FrequencyPojo> frequencies = resultMapper.toPOJO(queryResult, FrequencyPojo.class);
				 if (frequencies.size() > 0) {
					 frequency = frequencies.get(0);
				 }
			}
		} catch (Exception e) {
			log.error("error while geting results " + e.getMessage());
		}
		return frequency;
	}
	
	public static PricePojo getPrice(final String routeId) {
		PricePojo pricePojo = null;
		try {
			if (null != routeId) {
				 log.info("Fetching price for the routeID: " + routeId);
				 Query query = new Query("select * from " + TableName.Price + " where RouteID = \'"+routeId+"\'", DbNames.mydb);
				 log.info("Query: " + query.getCommand());
				 QueryResult queryResult = influxDB.query(query);
				 InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
				 List<PricePojo> prices = resultMapper.toPOJO(queryResult, PricePojo.class);
				 if (prices.size() > 0) {
					 pricePojo = prices.get(0);
				 }
			}
		} catch (Exception e) {
			log.error("error while geting results " + e.getMessage());
		}
		return pricePojo;
	}
	
	public static List<String> getVendorIdList(final String routeId) {
		List<OrderPojo> vendors = Collections.emptyList();
		List<String> vendorList = Collections.emptyList();
		try {
			log.info("Fetching frequency for the routeID: " + routeId);
			 Query query = new Query("select last(Price) as Price, Count, VendorID from " + TableName.OpenOrder + " where RouteID = \'"+routeId+"\' group by VendorID", DbNames.mydb);
			 log.info("Query: " + query.getCommand());
			 QueryResult queryResult = influxDB.query(query);
			 InfluxWriteImplenetation influx = new InfluxWriteImplenetation();
			 InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
			 vendors = resultMapper.toPOJO(queryResult, OrderPojo.class);
			 if (null != vendors && vendors.size() > 0) {
				 vendorList = new ArrayList<>(vendors.size());
				 List<OrderPojo> orders = Collections.emptyList();
				 for (OrderPojo order : vendors) {
					 if (null != order && null != order.getVendorID()) {
						 orders = influx.getLatestCountForVendor(order.getVendorID(),routeId);
						 if (null != orders && orders.size() > 0) {
							 if (orders.get(0).getCount() >= 5) {
								 vendorList.add(order.getVendorID());
							 }
						 }
					 }
				 }
			 }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vendorList;
	}
	
	public static void  moveConfirmedOrderToConfirmTable(String routeID, String orderId) {
	    try {
		log.info("moving points from OpenOder to Confirm series for the RouteID :"+routeID);
		String command = "SELECT * INTO "+ TableName.ConfirmOrder +" FROM  "+ TableName.OpenOrder + " WHERE RouteID = \'"+routeID+"\' and OrderID = '" + orderId + "'";
		String deletecommond = "DROP series from "+ TableName.OpenOrder +  " WHERE RouteID = \'"+routeID+"\' and TypeOfAction != 'NOTIFY_VENDORS' ";
		Query q = new Query(command, DbNames.mydb);
		Query dq = new Query(deletecommond, DbNames.mydb);
		log.info("move :"+q.getCommand());
		log.info("drop :"+dq.getCommand());
		APS.influxDB.query(q);
		Thread.sleep(2000L);
		APS.influxDB.query(dq);
		log.info("moving points from OpenOder to Confirm series for the RouteID :"+routeID  +"is successfull");
	    } catch (Exception e) {
		log.error("error during moving open to confirm series ");
		e.printStackTrace();
	    }
	    
	}
	
	public static OrdersCount getOrdersCount(final String routeId) {
		OrdersCount order = null;
		if (null != routeId && !"".equals(routeId)) {
			 log.info("Fetching order counts for the routeID: " + routeId);
			 Query query = new Query("select * from " + TableName.OrderCount + " where RouteID = \'"+routeId+"\'", DbNames.mydb);
			 log.info("Query: " + query.getCommand());
			 QueryResult queryResult = influxDB.query(query);
			 InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
			 List<OrdersCount> orders = resultMapper.toPOJO(queryResult, OrdersCount.class);
			 if (null != orders && orders.size() > 0) {
				 order = orders.get(0);
			 }
		}
		return order;
	}
	
	public static int checkCompletedOrders(final String routeId) {
		int orderCount = 0;
		if (null != routeId && !"".equals(routeId)) {
			 log.info("Fetching order counts for the routeID: " + routeId);
			 Query query = new Query("select Count(Distinct OrderID) from " + TableName.ConfirmOrder + " where RouteID = \'"+routeId+"\'", DbNames.mydb);
			 log.info("Query: " + query.getCommand());
			 QueryResult queryResult = influxDB.query(query);
			 if (null != queryResult) {
				 List<Result> results = queryResult.getResults();
				 if (results.size() > 0 && null != results.get(0).getSeries()) {
					 List<Series> series = results.get(0).getSeries();
					 if (series.size() > 0 && null != series.get(0).getValues()) {
						 List<List<Object>> values = series.get(0).getValues();
						 if (values.size() > 0 && null != values.get(0)) {
							 List<Object> value = values.get(0);
							 if (value.size() > 0 && null != value.get(1)) {
								 orderCount = (int) Double.parseDouble(value.get(1).toString());
							 }
						 }
					 }
				 }
			 }
		}
		return orderCount;
	}

}

