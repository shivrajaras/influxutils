
package com.kite.influx.templateImp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;
import com.kite.influx.APS;
import com.kite.influx.pojo.FrequencyPojo;
import com.kite.influx.pojo.OrderPojo;
import com.kite.influx.pojo.OrdersCount;
import com.kite.influx.pojo.PricePojo;
import com.kite.influx.pojo.SurgePojo;
import com.kite.influx.template.InfluxUtil;
import com.kite.influx.utils.DbNames;
import com.kite.influx.utils.TableName;
import com.kite.influx.utils.Util;

/**
 * @author shivraj
 *
 */
public class InfluxWriteImplenetation implements InfluxUtil {

   
    private static final long serialVersionUID = 1L;
    static final Log log = LogFactory.getLog(InfluxWriteImplenetation.class);


  
    /* (non-Javadoc)
     * @see com.kite.influx.template.InfluxUtil#insertData(com.kite.influx.pojo.OrderPojo)
     */
    @Override
    public void insertData(OrderPojo influxPoJo) throws Exception{
	Map<String, Object> map = null;
	Map<String, String> tags = null;
	    if (influxPoJo != null) {
		map = Util.getMap(influxPoJo);
		tags = Util.getTagMap(influxPoJo);
		APS.writeData(map, tags, TableName.OpenOrder);
	    }else {
		log.error("object is null");
		throw new NullPointerException("influx pojo is null");
	    }
	

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.kite.influx.template.InfluxUtil#insertData(com.kite.influx.pojo.
     * OrderPojo, java.util.Map)
     */
    @Override
    public void insertData(OrderPojo influxPoJo, Map<String, Object> extraData) throws Exception{

	Map<String, Object> map = null;
	Map<String, String> tags = null;
	    if (influxPoJo != null) {
		map = Util.getMap(influxPoJo);
		tags = Util.getTagMap(influxPoJo);
		if(extraData != null && !extraData.isEmpty()) {
		    map.putAll(extraData);
		}else {
		    log.warn("extra user map is null or empty");
		}
		APS.writeData(map, tags, TableName.OpenOrder);
	    }else {
		log.error("object is null");
		throw new NullPointerException("influx pojo is null");
	    }
	
    }
    
    public List<OrderPojo> getLatestPriceForAOrderGroupByVendor(String routeID) {
	//'ORD17005003'
	log.info("getting latest price for "+routeID +" routeID");
	List<OrderPojo> pojolist = new ArrayList<OrderPojo>();
	List<OrderPojo>  price = localgetLatestPriceForAOrderGroupByVendor(routeID);
	price.forEach((p) ->{
	    List<OrderPojo> countpojo= getLatestCountForVendor(p.getVendorID(),routeID);
	    if(!countpojo.isEmpty()) {
//		    OrderPojo po = new OrderPojo();
//		    po.setOrderID(p.getOrderID());
//		    po.setVendorID(p.getVendorID());
//		    po.setCount(countpojo.get(0).getCount());
//		    po.setPrice(p.getPrice());
//		    po.setTime(p.getTime());
	    	
	    	p.setCount(countpojo.get(0).getCount());
		}
	    pojolist.add(p);
		
		
	    
	});
	return pojolist;
    }
    
    private  List<OrderPojo> localgetLatestPriceForAOrderGroupByVendor(String routeID) {
   	//'ORD17005003'
   	String ss = "select time, LAST(Price) as Price, VendorID, OrderID, IncFrequency, IncFrequency2, DecFrequency, DecFrequency2, DateRequired, PendingOrders, TotalOrders, Count, "
   			+ "TypeOfAction, MarketID, TypeOfMaterial, TypeOfTruck, ToLocation, FromLocation, RouteID from OpenOrder WHERE RouteID = \'"+routeID+"\' Group By VendorID;";
   	
   	log.info("Query: " + ss);
   	
   	Query q = new Query(ss, DbNames.mydb);
   	QueryResult queryResult = APS.influxDB.query(q);
   	InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
   	List<OrderPojo> l = resultMapper.toPOJO(queryResult, OrderPojo.class);
   	//System.out.println(l);
   	return l;
   	}
       
       public  List<OrderPojo>  getLatestCountForVendor(String vid, String routeID){
   	//String count = "select SUM(Count) as Count from OpenOrder WHERE OrderID = \'"+OrderID+"\' Group By VendorID;";
   	String count = "select SUM(Count) as Count from OpenOrder WHERE VendorID =\'"+vid+"\' and RouteID =\'"+routeID+"\' and TypeOfAction = 'VIEW'";
   	Query q = new Query(count, DbNames.mydb);
   	QueryResult queryResult = APS.influxDB.query(q);
   	InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
   	List<OrderPojo> l = resultMapper.toPOJO(queryResult, OrderPojo.class);
   	//System.out.println(l);
   	return l;
       }
    
	@Override
	public void insertData(FrequencyPojo frequencyPojo) throws Exception {
		Map<String, Object> map = null;
		Map<String, String> tags = null;
		if (null != frequencyPojo) {
			map = Util.getFrequencyMap(frequencyPojo);
			tags = Util.getFrequencyTagMap(frequencyPojo);
			APS.writeData(map, tags, TableName.TimerFrequency);
		} else {
			log.error("Object is null");
			throw new NullPointerException("Frequency pojo is null");
		}

	}
	
	@Override
	public void insertData(PricePojo pricePojo) throws Exception {
		Map<String, Object> map = null;
		Map<String, String> tags = null;
		if (null != pricePojo) {
			map = Util.getPriceMap(pricePojo);
			tags = Util.getPriceTagMap(pricePojo);
			APS.writeData(map, tags, TableName.Price);
		} else {
			log.error("Object is null");
			throw new NullPointerException("Price pojo is null");
		}

	}
	//Anush Start
	@Override
	public void insertData(SurgePojo surgePojo) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = null;
		Map<String, String> tags = null;
		if (null != surgePojo) {
			map = Util.getSurgeMap(surgePojo);
			tags = Util.getSurgeTagMap(surgePojo);
			APS.writeData(map, tags, TableName.Surge);
		} else {
			log.error("Object is null");
			throw new NullPointerException("Surge pojo is null");
		}
	}
	//Anush End
	
	@Override
	public void writePendingAndTotalOrdersForRouteIntoOrderCount(OrdersCount ordersCount) throws Exception {
	    	Map<String, Object> map = null;
		Map<String, String> tags = null;
		if (null != ordersCount) {
			map = Util.getPriceMap(ordersCount);
			tags = Util.getPriceTagMap(ordersCount);
			Query q =new Query("DROP series from "+TableName.OrderCount +" where RouteID = \'"+ordersCount.getRouteID()+"\'", DbNames.mydb);
			log.info("command :"+q.getCommand());
			APS.influxDB.query(q);
			APS.writeData(map, tags, TableName.OrderCount);
		} else {
			log.error("Object is null");
			throw new NullPointerException("Price pojo is null");
		}
	}
	
	
}
