
/**
 * 
 */
package com.kite.influx.template;

import java.io.Serializable;
import java.util.Map;

import com.kite.influx.pojo.FrequencyPojo;
import com.kite.influx.pojo.OrderPojo;
import com.kite.influx.pojo.OrdersCount;
import com.kite.influx.pojo.PricePojo;
import com.kite.influx.pojo.SurgePojo;

/**
 * @author shivraj
 *
 */
public interface InfluxUtil extends Serializable {

    public void insertData(OrderPojo influxPoJo) throws Exception;
    
    public void insertData(OrderPojo influxPoJo ,Map<String ,Object> extraData) throws Exception;
    
    public void insertData(FrequencyPojo influxPoJo) throws Exception;
    public  void writePendingAndTotalOrdersForRouteIntoOrderCount(OrdersCount ordersCount) throws Exception;
    
    public void insertData(PricePojo influxPoJo) throws Exception;
  //Anush Start
    public void insertData(SurgePojo surgePojo) throws Exception;
  //Anush End
}

