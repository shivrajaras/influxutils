/*

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import com.opencsv.CSVReader;


public class InfluxDbTest {

    static InfluxDB influxDB = InfluxDBFactory.connect("http://kitedb.cloudapp.net:8086", "kite", "meratransport");
    static String dbName = "mydb";

    static String query = "insert table id=12,name=\"temp\",place=\"delhi\",price=12.0";

    public static void updateInfluxOrder(String influxStr){
           influxDB.write(dbName, "autogen", InfluxDB.ConsistencyLevel.ONE, influxStr );
           
    }
    
    public static void main(String[] args) throws IOException, InterruptedException {
	
	CSVReader reader = new CSVReader(new FileReader(new File("/home/shivraj/java/INFLUX/Influx.csv")));
//OrderID,RouteID,From,To,TypeOfTruck,TypeOfMaterial,VendorID,MarketID,TypeOfAction,Price,Count
	String[] header = reader.readNext();
	List<String[]> list = reader.readAll();
	System.out.println( list.size());
	Map<String,Object> map = new HashMap<String,Object>();
	Map<String,String> tags = new HashMap<String,String>();

	for(String[] s: list) {
	    for(int i=0;i<s.length;i++) {
		if(i < s.length-2) {
		    System.out.println(header[i]+"\t"+ s[i]);
		tags.put(header[i], s[i]);
		}else {
		    System.out.println(header[i]+"\t"+ s[i]);
		    map.put(header[i], Double.parseDouble(s[i]));

		}
	    }
	    Thread.currentThread().sleep(1000L);
	    writeData(map,tags,"OpenOrder");
		
	}
	
	
	map.forEach((k,v) ->{
		System.out.println( k +"\t"+ v);

	});
	tags.forEach((k,v) ->{
		System.out.println( k +"\t"+ v);

	});
	
	
	
	
	
	String table = "table";
	Map<String,Object> map = new HashMap<String,Object>();
	map.put("orderID", "ORD123");
	map.put("place", "bengalore");
	
	Map<String,String> tags = new HashMap<String,String>();
	tags.put("id", "1");
	tags.put("route", "from-to");
	Point p = Point.measurement("table").fields(map).tag(tags).time(System.currentTimeMillis(), TimeUnit.MILLISECONDS).build();
	
	APS.writeData(map,tags,table);
	
	//System.out.println(APS.getResults(new Query("select * from table" ,dbName)));
	//System.out.println(APS.influxDB.query(new Query("select * from table" ,dbName)).getResults());
	
	QueryResult re = influxDB.query(new Query(" select * from httpd limit 20", "_internal"), TimeUnit.MINUTES);
	System.out.println(re.getResults());
	
	InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
	List<Object> cpuList = resultMapper.toPOJO(re, Object.class);
	System.out.println(cpuList);
	//System.out.println(influxDB.query(new Query("select * from httpd", "_internal"), TimeUnit.MINUTES));
    }
    
    public synchronized static void writeData(Map<String, Object> map, Map<String, String> tags, String table) {
	try {
	    Point p = Point.measurement(table).fields(map).tag(tags).time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
	    	.build();
	    influxDB.write(dbName, "meratransport", p);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

}
*/